﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionApp_Backend.Models
{
    public class Answer
    {
        public int AnswerId { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
        public int Timestamp { get; set; }
        public string UserName { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
