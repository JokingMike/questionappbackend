﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionApp_Backend.Models
{
    public class Question
    {
        [Key]
        public int QuestionId { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int Timestamp { get; set; }
        public string Subject { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Answer> Answers { get; set; }
    }
}
