﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionApp_Backend.Models
{
    public enum Subject
    {
        Français,
        Informatique,
        Philosophie
    }
}
