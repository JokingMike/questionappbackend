﻿using QuestionApp_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionApp_Backend.Repositories
{
    public interface IAnswerRepository
    {
        Answer GetAnswer(int Id);
        IEnumerable<Answer> GetAllAnswersFromQuestion(int Id);
        Answer Add(Answer answer);
        Answer Update(Answer answerChanges);
        Answer Delete(int Id);
    }
}
