﻿using QuestionApp_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionApp_Backend.Repositories
{
    public interface IQuestionRepository
    {
        Task<Question> GetQuestion(int Id);
        Task<IEnumerable<Question>> GetAllQuestions();
        Task<IEnumerable<Question>> GetAllQuestionsFromSubject(string subject);
        Question Add(Question question);
        Question Update(Question questionChanges);
        Question Delete(int Id);
    }
}
