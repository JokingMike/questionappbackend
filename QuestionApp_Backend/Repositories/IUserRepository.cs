﻿using QuestionApp_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionApp_Backend.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUser(int Id);
        Task<IEnumerable<User>> GetAllUsers();
        User Add(User user);
        User Update(User userChanges);
        User Delete(int Id);
    }
}
