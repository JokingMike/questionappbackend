﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QuestionApp_Backend.Data;
using QuestionApp_Backend.Models;

namespace QuestionApp_Backend.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly DataContext _context;

        public QuestionRepository(DataContext context)
        {
            _context = context;
        }


        public Question Add(Question question)
        {
            _context.Questions.Add(question);
            _context.SaveChanges();
            return question;
        }

     
        public Question Delete(int Id)
        {
            Question question = _context.Questions.Find(Id);
            if(question != null)
            {
                _context.Questions.Remove(question);
                _context.SaveChanges();
            }
            return question;
        }


        public IEnumerable<Question> GetAllQuestionsFromSubject(string subject)
        {
            var questions = from m in _context.Questions
                         select m;
            
            if (!String.IsNullOrEmpty(subject))
            {
                questions = questions.Where(s => s.Subject.Contains(subject));
            }

            return questions.ToList();
        }


        public Question Update(Question questionChanges)
        {
            var question = _context.Questions.Attach(questionChanges);
            question.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return questionChanges;
        }


        async Task<IEnumerable<Question>> IQuestionRepository.GetAllQuestions()
        {
            var questions = await _context.Questions.ToListAsync();
            return questions ;
        }


        async Task<IEnumerable<Question>> IQuestionRepository.GetAllQuestionsFromSubject(string subject)
        {
            var questions = from m in _context.Questions
                            select m;

            if (!String.IsNullOrEmpty(subject))
            {
                questions = questions.Where(s => s.Subject.Contains(subject));
            }

            return await questions.ToListAsync();
        }


        async Task<Question> IQuestionRepository.GetQuestion(int Id)
        {
            var question = await _context.Questions.FindAsync(Id);
            return question;
        }
    }
}
